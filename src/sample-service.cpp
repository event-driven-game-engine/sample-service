#include "engine.h"

int xyz;
struct SpaceBarSubscriber : InputSubscriber
{
    SpaceBarSubscriber () : InputSubscriber(InputDef::KeyCode(SDLK_SPACE))
	{
		printf("sample service is being loaded\n");
	}
	~SpaceBarSubscriber ()
	{
		printf("sample service is being unloaded\n");
	}
    int presses = 0;
    void Execute () override
    {
        presses++;
        printf("sample service strikes again\n");
    }
}
SpaceBarSubscriberInstance;
